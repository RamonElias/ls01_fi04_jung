﻿import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

class Fahrkartenautomat {
	
	//4 Scheine: 	50, 20, 10, 5
	//8 Münzen:		2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01

	//Standard 500€ Kasse
	static int einCentAnz 		= 200;	//2
	static int zweiCentAnz 		= 150;	//3
	static int fuenfCentAnz 	= 100;	//5
	static int zehnCentAnz 		= 100;	//10
	static int zwanzigCentAnz 	= 100;	//20
	static int fuenfzigCentAnz 	= 100;	//50
	static int einEuroAnz 		= 50;	//50
	static int zweiEuroAnz 		= 50;	//100
	static int fuenfEuroAnz 	= 20;	//100
	static int zehnEuroAnz 		= 5;	//50
	static int zwanzigEuroAnz 	= 3;	//60
	static int fuenfzigEuroAnz 	= 1; 	//50
	
	static int rohlinge			= 5; //Anzahl der bedruckbaren Ticketsrohlinge
	
	public static void main(String[] args) {

		//Endlosschleife zum dauerhaften Betrieb des Automaten
		while (true) {
			double rückgabebetrag = fahrkartenBezahlen(fahrkartenbestellungErfassen());

			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(rückgabebetrag);

			System.out.println("\n Vergessen Sie nicht, den Fahrschein\n" 
					+ " vor Fahrtantritt entwerten zu lassen!\n"
					+ " Wir wünschen Ihnen eine gute Fahrt.\n");
		}
	}

	private static double fahrkartenbestellungErfassen() {

		Scanner tastatur = new Scanner(System.in);
		String[] fahrkartenName = {
				"Einzelfahrschein Berlin AB        ", 
				"Einzelfahrschein Berlin BC        ",
				"Einzelfahrschein Berlin ABC       ",
				"Kurzstrecke                       ",
				"Tageskarte Berlin AB              ",
				"Tageskarte Berkin BC              ",
				"Tageskarte Berlin ABC             ",
				"Kleingruppe-Tageskarte Berlin AB  ",
				"Kleingruppe-Tageskarte Berlin BC  ",
				"Kleingruppe-Tageskarte Berlin ABC "};
		double[] zuZahlenderBetrag = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
		int[] ticketAnzahl = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //Identische Anzahl zu zuZahlenderBetrag
		int ticketAnzahlWahl;
		double schulden = 0;
		int ticketArt = 0;
		
		
		fahrkartenMenu(false, zuZahlenderBetrag, (int)zuZahlenderBetrag.length, fahrkartenName);		
		ticketArt = tastatur.nextInt();
		
		if (ticketArt == 666) {
			adminMenu();
		}
		
		//Beschränkung der eingabe
		//Länge der Liste an Tickets + 1(von 0-9 zu 1-10 in der eingabe)
		while(ticketArt != zuZahlenderBetrag.length+1) {
			
			if (ticketArt > 0 && ticketArt <= zuZahlenderBetrag.length+1) {
					
				//Ticket Anzahl
				//Die Ticket Anzahl kann nicht negativ sein und nicht größer als 10
				System.out.print("\n Anzahl der Tickets (Max. 10): ");
				ticketAnzahlWahl = tastatur.nextInt();
					
				if (ticketAnzahlWahl < 0) {
					System.out.println("\nFALSCHE EINGABE!\n" 
						+ "=========================\n"
						+ "Eine negative Anzahl Tickets ist nicht Buchbar\n"
						+ "Es wird EIN Ticket gebucht.\n");
					ticketAnzahl[ticketArt-1]++;
					testRohlinge(1);
				} else if (ticketAnzahlWahl > 10) {
					System.out.println("\nFALSCHE EINGABE!\n" 
						+ "=========================\n"
						+ "Es können nur bis zu 10 Tickets gebucht werden.\n"
						+ "Es wird EIN Ticket gebucht.\n");
					ticketAnzahl[ticketArt-1]++;
					testRohlinge(1);
				} else {
					ticketAnzahl[ticketArt-1] += ticketAnzahlWahl;
					testRohlinge(ticketAnzahlWahl);
				}
			} else {
				System.out.println("\nFALSCHE EINGABE!\n" 
						+ "=========================\n"
						+ "Diese Eingabe ist nicht im Angebot enthalten,\n" 
						+ "bitte wählen Sie ein gültiges Ticket:");
			}
			
			fahrkartenMenu(true, zuZahlenderBetrag, (int)zuZahlenderBetrag.length+1, fahrkartenName);
				
			ticketArt = tastatur.nextInt();
			
		}
		
		for(int i = 0; i <= zuZahlenderBetrag.length-1; i++) {
			schulden += zuZahlenderBetrag[i]*ticketAnzahl[i];
		}

		//Rückgabe des Gesamtpreises
		return schulden;
		
	}
	
	private static void testRohlinge(int insgesamt) {
		
		if(rohlinge-insgesamt < 0) {
			System.out.print("LEIDER BESTEHT EIN PROBLEM MIT DEM AUTOMATEN.\n"
					+ "EIN MITARBEITER IST BEREITS INFORMIERT.\n"
					+ "SIE ERHALTEN IHR GELD ZURÜCK!\n\n"
					+ "DER FAHRKARTENAUTOMAT IST AUßER BETRIEB!\n\n\n\n\n");
	
			warte(60000);
			System.exit(0);
		} else {
			rohlinge -= insgesamt;
		}
	}
	
	//Angabe aller Fahrkarten Arten und dem Fahrkartenbestellvorgang
	private static void fahrkartenMenu(boolean firstTime, double[] preis, int zurZahlung, String[] ticketName) {

		
		//Nur beim ersten durchlauf soll "Fahrkartenbestellung" augegeben werden
		if (firstTime == false) {
			System.out.print(""
					+ "Fahrkartenbestellvorgang:\n"
					+ "=========================\n");
					System.out.printf("Nr.%11s%31s\n", "Fahrkarte", "Preis");
			for (int i = 0; i <= 9; i++) {
				System.out.printf(" %-4d%s%6.2f€\n", i+1, ticketName[i], preis[i]);
			}
			System.out.print("Wählen Sie ihre Nummer: ");
		} else {
			System.out.print("=========================\n");
			System.out.printf("Nr.%11s%31s\n", "Fahrkarte", "Preis");
	for (int i = 0; i <= 9; i++) {
		System.out.printf(" %-4d%s%6.2f€\n", i+1, ticketName[i], preis[i]);
	}
		System.out.print(" Wählen Sie die " + zurZahlung + " um zu bezahlen.\n"
				+ "Wählen Sie ihre Nummer: ");
		}
	}
	
	//Bezhalvorgang
	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		Scanner scan = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		double rückgabebetrag;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("\n Noch zu zahlen : %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print(" Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = scan.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rückgabebetrag;
	}

	//Die Ausgabe der Bestellten Fahrscheine
	private static void fahrkartenAusgeben() {

		System.out.print("\n Fahrschein wird ausgegeben\n ");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	//Berechnung des Rückgelds
	private static void rueckgeldAusgeben(double rückgabebetrag) {
		
		String[] dreiMuenzenEinheit = new String[3];
		int[] dreiMuenzenWert = new int[3];
		int nurDreiMuenzen = 0;
		String s = "*";

		//Rückgeldbetrag wird abgerrechnet aber nur wenn die Kasse Flüssig ist
		if ((rückgabebetrag > 0.0) && (kasseIstValide(rückgabebetrag*100))) {
			System.out.printf(" Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);
			System.out.println(" wird in folgenden Münzen ausgezahlt:");

			rückgabebetrag *= 100;

			//While Schleifen die passende Münzen erstellen nachdem geld aus der kasse genommen wurde für das rückgeld.
			while ((Math.round(rückgabebetrag) >= 5000) && (fuenfzigEuroAnz > 0)) // 50 EURO-Münzen
			{
				//Ausgabe der drei Münzen füllen
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "EURO";
				dreiMuenzenWert[nurDreiMuenzen%3] = 50;
				nurDreiMuenzen++;
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				//Rechnung des Restgeldbetrags
					rückgabebetrag -= 5000;
				//Münze wird aus der Kasse entfernt
					fuenfzigEuroAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 2000) && (zwanzigEuroAnz > 0)) // 20 EURO-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "EURO";
				dreiMuenzenWert[nurDreiMuenzen%3] = 20;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 2000;
				zwanzigEuroAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 1000) && (zehnEuroAnz > 0)) // 10 EURO-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "EURO";
				dreiMuenzenWert[nurDreiMuenzen%3] = 10;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 1000;
				zehnEuroAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 500) && (fuenfEuroAnz > 0)) // 5 EURO-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "EURO";
				dreiMuenzenWert[nurDreiMuenzen%3] = 5;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 500;
				fuenfEuroAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 200) && (zweiEuroAnz > 0)) // 2 EURO-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "EURO";
				dreiMuenzenWert[nurDreiMuenzen%3] = 2;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 200;
				zweiEuroAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 100) && (einEuroAnz > 0)) // 1 EURO-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "EURO";
				dreiMuenzenWert[nurDreiMuenzen%3] = 1;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 100;
				einEuroAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 50) && (fuenfzigCentAnz > 0)) // 50 CENT-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "CENT";
				dreiMuenzenWert[nurDreiMuenzen%3] = 50;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 50;
				fuenfzigCentAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 20) && (zwanzigCentAnz > 0)) // 20 CENT-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "CENT";
				dreiMuenzenWert[nurDreiMuenzen%3] = 20;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 20;
				zwanzigCentAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 10) && (zehnCentAnz > 0)) // 10 CENT-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "CENT";
				dreiMuenzenWert[nurDreiMuenzen%3] = 10;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 10;
				zehnCentAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 5) && (fuenfCentAnz > 0))// 5 CENT-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "CENT";
				dreiMuenzenWert[nurDreiMuenzen%3] = 5;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 5;
				fuenfCentAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 2) && (zweiCentAnz > 0))// 2 CENT-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "CENT";
				dreiMuenzenWert[nurDreiMuenzen%3] = 2;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 2;
				zweiCentAnz--;
			}
			while ((Math.round(rückgabebetrag) >= 1) && (einCentAnz > 0))// 1 CENT-Münzen
			{
				dreiMuenzenEinheit[nurDreiMuenzen%3] = "CENT";
				dreiMuenzenWert[nurDreiMuenzen%3] = 1;
				nurDreiMuenzen++;	
				if( (nurDreiMuenzen)%3 == 0 ) {
					muenzeAusgeben(dreiMuenzenWert, dreiMuenzenEinheit);
				}
				rückgabebetrag -= 1;
				einCentAnz--;
			}
			
			//Wenn am ende der rechnung münzen weniger als 3 übrigbleiben müssen
			//diese extra ausgegeben werden.
			if(nurDreiMuenzen%3 == 2) {

				System.out.printf("     * * *       * * *\n"
						+ "   *       *   *       *\n"
						+ "%3s%6d%4s%2s%6d%4s\n"
						+ "  *   %S  * *   %S  *\n"
						+ "   *       *   *       *\n"
						+ "     * * *       * * *\n"
						+ "", s, dreiMuenzenWert[0], s, s, dreiMuenzenWert[1], s, 
						dreiMuenzenEinheit[0], dreiMuenzenEinheit[1]);
			} else if(nurDreiMuenzen%3 == 1) {
				
				System.out.printf("     * * *\n"
						+ "   *       *\n"
						+ "%3s%6d%4s\n"
						+ "  *   %S  *\n"
						+ "   *       *\n"
						+ "     * * *\n"
						+ "", s, dreiMuenzenWert[0], s, 
						dreiMuenzenEinheit[0]);
			}
		}
	}

	//ein Timer der das Programm unterbricht
	private static void warte(int millisekunde) {

		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//Die Ausgabe der münzen in Formtierterr form
	private static void muenzeAusgeben(int[] betrag, String[] einheit) {
		
		String s = "*";
		
		System.out.printf("     * * *       * * *       * * *\n"
				+ "   *       *   *       *   *       *\n"
				+ "%3s%6d%4s%2s%6d%4s%2s%6d%4s\n"
				+ "  *   %S  * *   %S  * *   %S  *\n"
				+ "   *       *   *       *   *       *\n"
				+ "     * * *       * * *       * * *\n"
				+ "", s, betrag[0], s, s, betrag[1], s, s, betrag[2], s, einheit[0], einheit[1], einheit[2]);	
	}
	
	//Admin menü zum steuern der Wechselgeldkasse und den Rohlingen
	private static void adminMenu() {
		
		Scanner tastatur = new Scanner(System.in);
		int loop = 0;
		int fillItUp;
		int fillItUpWith;
		
		while (loop != 9) {
			System.out.print("\n\n"
				+ "Willkommen im ADMIN Menü.\n"
				+ "(1) Möchten Sie die Kasse einsehen?\n"
				+ "(2) Leerung der Kasse.\n"
				+ "(3) Aufüllen der Kasse.\n"
				+ "(4) Rohlinge einsehen.\n"
				+ "(5) Leerung des Rohlingsfach.\n"
				+ "(6) Auffüllen des Rohlingsfach.\n\n"
				+ "(9) Beenden des ADMIN Menüs.\n");
	
		loop = tastatur.nextInt();
		
		switch(loop) {
		
			case 1:
				System.out.printf(" Betrag   | Anzahl\n"
								+ "  50 €    |%5d\n"
								+ "  20 €    |%5d\n"
								+ "  10 €    |%5d\n"
								+ "   5 €    |%5d\n"
								+ "   2 €    |%5d\n"
								+ "   1 €    |%5d\n"
								+ "  50 Cent |%5d\n"
								+ "  20 Cent |%5d\n"
								+ "  10 Cent |%5d\n"
								+ "   5 Cent |%5d\n"
								+ "   2 Cent |%5d\n"
								+ "   1 Cent |%5d\n", fuenfzigEuroAnz, zwanzigEuroAnz, zehnEuroAnz, fuenfEuroAnz, zweiEuroAnz, einEuroAnz, fuenfzigCentAnz, zwanzigCentAnz, zehnCentAnz, fuenfCentAnz, zweiCentAnz, einCentAnz);
			break;
			
			case 2:
				einCentAnz 		= 0;
				zweiCentAnz 	= 0;
				fuenfCentAnz 	= 0;
				zehnCentAnz 	= 0;
				zwanzigCentAnz 	= 0;
				fuenfzigCentAnz = 0;
				einEuroAnz 		= 0;
				zweiEuroAnz 	= 0;
				fuenfEuroAnz 	= 0;
				zehnEuroAnz 	= 0;
				zwanzigEuroAnz 	= 0;
				fuenfzigEuroAnz = 0; 
				
				System.out.print("\nKasse wurde geleert!\n\n");
			break;
			
			case 3:
				System.out.print("Welches Fach möchten Sie befüllen?\n"
								+ "(1)  50 Euro\n"
								+ "(2)  20 Euro\n"
								+ "(3)  10 Euro\n"
								+ "(4)   5 Euro\n"
								+ "(5)   2 Euro\n"
								+ "(6)   1 Euro\n"
								+ "(7)  50 Cent\n"
								+ "(8)  20 Cent\n"
								+ "(9)  10 Cent\n"
								+ "(10)  5 Cent\n"
								+ "(11)  2 Cent\n"
								+ "(12)  1 Cent\n\n");
				fillItUp = tastatur.nextInt();
				
				System.out.print("Welche Anzahl fügen Sie hinzu?\n");
				fillItUpWith = tastatur.nextInt();
				
				switch(fillItUp){
				
				case 1:	fuenfzigEuroAnz += fillItUpWith; break;
				case 2:	zwanzigEuroAnz 	+= fillItUpWith; break;
				case 3:	zehnEuroAnz 	+= fillItUpWith; break;
				case 4:	fuenfEuroAnz 	+= fillItUpWith; break;
				case 5: zweiEuroAnz 	+= fillItUpWith; break;
				case 6:	einEuroAnz		+= fillItUpWith; break;
				case 7:	fuenfzigCentAnz += fillItUpWith; break;
				case 8:	zwanzigEuroAnz 	+= fillItUpWith; break;
				case 9:	zehnCentAnz 	+= fillItUpWith; break;
				case 10: fuenfCentAnz 	+= fillItUpWith; break;
				case 11: zweiCentAnz 	+= fillItUpWith; break;
				case 12: einCentAnz 	+= fillItUpWith; break;
				}
				System.out.print("Kasse wurde gefüllt.\n\n");
			break;
			
			case 4: System.out.print("Anzahl Vorhandenen Rohlinge: " + rohlinge + "\n"); 
			break;
			
			case 5: 
				rohlinge = 0;
				System.out.print("Rohlingfach wurde geleert.\n"
								+"Bitte vor dem start Befüllen!");
			break;
			
			case 6:
				System.out.print("Geben Sie bitte die Anzahl der hinzugefügten Rohlinge an:");
				rohlinge += tastatur.nextInt();
			break;
			
			default:
			}
		}
	}
	
	
	//Test ob die Kasse genug Wechselgeld für den Betrag besitzt
	private static boolean kasseIstValide(double betrag) {
		
		
		int token;
		
		token = fuenfzigEuroAnz;
		while ((Math.round(betrag) >= 5000) && (token > 0)) // 50 EURO-Münzen
		{
				betrag -= 5000;
			//Münze wird aus der Kasse entfernt
				token--;
		}
		token = zwanzigEuroAnz;
		while ((Math.round(betrag) >= 2000) && (token > 0)) // 20 EURO-Münzen
		{
			betrag -= 2000;
			token--;
		}
		token = zehnEuroAnz;
		while ((Math.round(betrag) >= 1000) && (token > 0)) // 10 EURO-Münzen
		{
			betrag -= 1000;
			token--;
		}
		token = fuenfEuroAnz;
		while ((Math.round(betrag) >= 500) && (token > 0)) // 5 EURO-Münzen
		{
			betrag -= 500;
			token--;
		}
		token = zweiEuroAnz;
		while ((Math.round(betrag) >= 200) && (token > 0)) // 2 EURO-Münzen
		{
			betrag -= 200;
			token--;
		}
		token = einEuroAnz;
		while ((Math.round(betrag) >= 100) && (token > 0)) // 1 EURO-Münzen
		{
			betrag -= 100;
			token--;
		}
		token = fuenfzigCentAnz;
		while ((Math.round(betrag) >= 50) && (token > 0)) // 50 CENT-Münzen
		{
			betrag -= 50;
			token--;
		}
		token = zwanzigCentAnz;
		while ((Math.round(betrag) >= 20) && (token > 0)) // 20 CENT-Münzen
		{
			betrag -= 20;
			token--;
		}
		token = zehnCentAnz;
		while ((Math.round(betrag) >= 10) && (token > 0)) // 10 CENT-Münzen
		{
			betrag -= 10;
			token--;
		}
		token = fuenfCentAnz;
		while ((Math.round(betrag) >= 5) && (token > 0))// 5 CENT-Münzen
		{
			betrag -= 5;
			token--;
		}
		token = zweiCentAnz;
		while ((Math.round(betrag) >= 2) && (token > 0))// 2 CENT-Münzen
		{
			betrag -= 2;
			token--;
		}
		token = einCentAnz;
		while ((Math.round(betrag) >= 1) && (token > 0))// 1 CENT-Münzen
		{
			betrag -= 1;
			token--;
		}
		
		if(Math.round(betrag) > 0) {
			System.out.print("LEIDER BESTEHT EIN PROBLEM MIT DEM AUTOMATEN.\n"
							+ "EIN MITARBEITER IST BEREITS INFORMIERT.\n"
							+ "SIE ERHALTEN IHR GELD ZURÜCK!\n\n"
							+ "DER FAHRKARTENAUTOMAT IST AUßER BETRIEB!\n\n\n\n\n");
			
			warte(60000);
			System.exit(0);
			return false;
		} else {
			return true;
		}
	}
}

/*
1. die Anzahl der Geldscheine bzw. Münzen sollen verwaltet werden.
2. Fahrkarten können nur gekauft werden können, wenn das passende Wechselgeld vorhanden ist.
3. Ein Administrations-Menü, mit dem man die Kasse leeren bzw. mit Geld auffüllen kann.
	Der Automat hat nur eine bestimmte Anzahl von Ticket-Rohlingen. 
4. Sollten nicht genügend Rohlinge vorrätig sein, um Tickets zu drucken, 
	soll ein Kaufvorgang nicht möglich sein und dies dem Kunden mitgeteilt werden.
*/
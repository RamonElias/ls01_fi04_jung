import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;
import javax.swing.plaf.synth.SynthToggleButtonUI;

public class Mittelwert {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Werte für x und y festlegen:
		// ===========================
		double x = liesDoubleWertEin("Wert eins: ");
		double y = liesDoubleWertEin("Wert zwei: ");
		double m;

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		m = mittelwertBerechnen(x, y);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);

	}

	private static double mittelwertBerechnen(double x, double y) {
		double m;
		m = (x + y) / 2.0;
		return m;
	}

	public static double liesDoubleWertEin(String frage) {
		double zahl;
		Scanner tastatur = new Scanner(System.in);
		System.out.println(frage);
		zahl = tastatur.nextDouble();
		return zahl;
	}
}
